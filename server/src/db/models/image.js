const mongoose = require("mongoose");

const imageSchema = new mongoose.Schema(
  {
    _id: {
      type: String,
      required: true,
    },
    title: {
      type: String,
      trim: true,
      required: true,
    },
    extension: {
      type: String,
      required: true,
    },
  },
  { timestamps: true }
);

imageSchema.index({ title: "text" });

const Image = mongoose.model("Image", imageSchema);

module.exports = Image;
