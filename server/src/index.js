const port = process.env.PORT || 8080;

const express = require("express");
const multer = require("multer");
const path = require("path");
const bodyParser = require("body-parser");
const fs = require("fs");
const { v4: uuidv4 } = require("uuid");
require("./db/mongoose");
const app = express();

const Image = require("./db/models/image");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, "../images")));
app.use(express.static(path.join(__dirname, "../build")));

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./images");
  },
  fileFilter: function (req, file, cb) {
    if (!file.originalname.match(/\.(png|jpg|jpeg)$/)) {
      cb(new Error("Please upload an image."));
    }

    cb(undefined, true);
  },
  filename: function (req, file, cb) {
    const { originalname } = file;
    var ext = originalname.substr(originalname.lastIndexOf(".") + 1);
    cb(null, `${uuidv4()}.${ext}`);
  },
});

var upload = multer({ storage });

app.get("/images", async (req, res) => {
  const searchTerm = req.query.searchTerm;
  try {
    let images;
    if (searchTerm) {
      images = await Image.find({ $text: { $search: searchTerm } });
    } else {
      images = await Image.find();
    }

    res.send(images);
  } catch (e) {
    res.status(400).send(e);
  }
});

app.get("/images/:searchTerm", async (req, res) => {
  const searchTerm = req.params.searchTerm;

  try {
    res.send(images);
  } catch (e) {
    res.status(400).send(e);
  }
});

app.post(
  "/images",
  upload.array("upload"),
  async (req, res) => {
    const { files } = req;
    const images = [];
    files.forEach(file => {
      const image = new Image({
        _id: file.filename.substr(0, file.filename.lastIndexOf(`.`)),
        title: file.originalname,
        extension: file.originalname.slice(
          file.originalname.lastIndexOf(".") + 1
        ),
      });
      images.push(image);
    });

    try {
      Image.create(images);
    } catch (e) {
      res.status(400).send(e);
    }
    res.send({ images });
  },
  (error, req, res, next) => {
    console.log(error);
    // Dealing with multer related errors
    res.status(400).send({ error: error.message });
  }
);

const deleteFromDbAndDisk = async imageId => {
  try {
    const image = await Image.findByIdAndDelete({ _id: imageId });
    fs.unlink(`./images/${imageId}.${image.extension}`, err => {
      if (err) console.log(err);
      else {
        console.log("success");
      }
    });
    return [true, undefined];
  } catch (e) {
    return [undefined, e];
  }
};

app.delete("/images", async (req, res) => {
  const { imageIds } = req.body;

  let errorFlag = false;
  for (let imageId of imageIds) {
    let [success, error] = await deleteFromDbAndDisk(imageId);
    if (error) {
      res.status(400).send(error);
      errorFlag = true;
      return;
    }
  }
  if (!errorFlag) res.send();
});

app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname, "build/index.html"));
});

app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
