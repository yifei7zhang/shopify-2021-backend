import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import GridList from "@material-ui/core/GridList";
import ImageGridItem from "./ImageGridItem";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-around",
    overflow: "hidden",
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    width: 1000,
    height: 1000,
  },
  titleBar: {
    background:
      "linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)",
  },
}));

const ImageGrid = ({ addToDelete, removeFromDelete, images }) => {
  const classes = useStyles();

  if (!images || images.length === 0) return null;

  return (
    <div className={classes.root}>
      <GridList cellHeight={300} className={classes.gridList}>
        {images.map(image => (
          <ImageGridItem
            key={image._id}
            addToDelete={addToDelete}
            removeFromDelete={removeFromDelete}
            image={image}
          />
        ))}
      </GridList>
    </div>
  );
};

export default ImageGrid;
