import React, { useState } from "react";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import GridListTileBar from "@material-ui/core/GridListTileBar";
import ListSubheader from "@material-ui/core/ListSubheader";
import IconButton from "@material-ui/core/IconButton";
import InfoIcon from "@material-ui/icons/Info";
import tileData from "./tileData";

import CheckBoxOutlineBlankIcon from "@material-ui/icons/CheckBoxOutlineBlank";
import CheckBoxIcon from "@material-ui/icons/CheckBox";
import ImageGrid from "./ImageGrid";

const ImageGridItem = ({ addToDelete, removeFromDelete, image }) => {
  const [deleteChecked, setDeleteChecked] = useState(false);

  const handleOnImageClicked = () => {
    if (!deleteChecked) {
      addToDelete(image._id);
    } else {
      removeFromDelete(image._id);
    }
    setDeleteChecked(!deleteChecked);
  };
  return (
    <GridListTile
      key={image._id}
      style={{ height: "250px", width: "250px", margin: "10px" }}
    >
      <img
        src={`${window.location.href}/${image._id}.${image.extension}`}
        alt={image.title}
        style={{ objectFit: "cover" }}
      />
      <GridListTileBar
        title={image.title}
        actionIcon={
          <IconButton aria-label={`select`} onClick={handleOnImageClicked}>
            {deleteChecked ? <CheckBoxIcon /> : <CheckBoxOutlineBlankIcon />}
          </IconButton>
        }
      />
    </GridListTile>
  );
};

export default ImageGridItem;
