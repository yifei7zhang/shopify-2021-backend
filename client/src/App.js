import React, { useState, useEffect } from "react";
import axios from "axios";
import "./App.css";
import Header from "./Header";
import ImageGrid from "./ImageGrid";
import Button from "@material-ui/core/Button";

const App = () => {
  const [toDelete, setToDelete] = useState([]);
  const [files, setFiles] = useState([]);
  const [images, setImages] = useState([]);
  useEffect(() => {
    const getImages = async () => {
      const images = await axios.get("/images");
      setImages(images.data);
    };

    getImages();
  }, []);

  const addToDelete = id => {
    if (toDelete.length === 0) {
      setToDelete([id]);
    } else {
      setToDelete([...toDelete, id]);
    }
  };

  const removeFromDelete = id => {
    const index = toDelete.indexOf(id);
    if (index > -1) {
      setToDelete([...toDelete].splice(index, 1));
    }
  };

  const fileUploadChange = e => {
    const files = e.target.files;
    setFiles(files);
  };

  const deleteImages = () => {
    axios.delete("/images", { data: { imageIds: toDelete } });
    setToDelete([]);
  };

  const onFormSubmit = async e => {
    e.preventDefault();
    const formData = new FormData();

    [...files].forEach(file => {
      formData.append("upload", file);
    });

    const res = await axios.post("/images", formData, {
      headers: { "Content-Type": "multipart/form-data" },
    });
  };

  return (
    <div>
      <Header setImages={setImages} />
      <form onSubmit={onFormSubmit}>
        <label htmlFor="upload-photo">
          <input
            style={{ display: "none" }}
            id="upload-photo"
            name="upload-photo"
            type="file"
            multiple
            onChange={fileUploadChange}
          />

          <Button color="secondary" variant="contained" component="span">
            Upload Photo(s)
          </Button>
        </label>
        <Button variant="contained" type="submit">
          Submit to repository
        </Button>
      </form>
      {[...files].map(file => {
        return <div>{file.name}</div>;
      })}
      <Button variant="contained" color="primary" onClick={deleteImages}>
        Delete Selected
      </Button>
      <ImageGrid
        addToDelete={addToDelete}
        removeFromDelete={removeFromDelete}
        images={images}
      />
    </div>
  );
};

export default App;
