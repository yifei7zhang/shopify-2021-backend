# Shopify Image Repo

Hey, there, thanks for checking out my shopify submission! Here's some basic info to get this up and running.

### Pre-requisites

- docker

Make sure that you are in the root directory of this repo, and follow the command below to start 2 docker containers; one for a mongodb database, and a nodejs/express backend.

```
docker-compose up -d
```

Aftewards, you will be able to see view the client at localhost:8080.

You will be able to add/remove images from the repository, as well as search through images by a keyword.

## Reflections on this project

While initially not liking the open-ended nature of the project prompt, I found after doing some reading and digging around, I got more and more excited to work on the challenge! Initially, I wanted to spend the large majority of time focused on the search features, as the reverse image search really stood out to me. In particular, I had heard of this package OpenCV4NodeJS, that would allow me to use OpenCV in a nodejs environment as well. However, I spent so long trying to figure out build errors and configuration differences, that I ended up scrapping that idea. In the future, I would probably want to learn Flask as well, so that I can take advantage of the more robust computer vision and other ML libraries that are more readily accessible through Flask.

The choice to store the images on disk instead of some cloud service was purely for convenience; but, when thinking about scalability, the clear next step would be to start storing these images on some sort of cloud service. I had a great time doing the project (even though I didn't fully get to implement all the search features that I wanted), and I hope you like it as well!
